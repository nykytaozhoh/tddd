# -*- coding: utf-8 -*-

"""
name: TimeDoctor Driven Development - hello wasted time!
author: NickOz
date: 16/03/2018
current version description: modeling employee's behaviour and activity
upcoming version description: take a screen of time doctor's asking popup
and manipulate it using pyautogui by clicking and confirming 'still working'
process
"""

import pyautogui
import os, random

# help time_doctor_worker avoiding messing up the
# whole working place by specifying boundaries
safe_frame = 200


# generates random seed for line int number
def random_line_number(limit):
    return int(random.random() * limit)


# generates random seed for time value
# in order to create pseudo-realistic
# motion behaviour
def random_time(start, end):
    return random.randrange(start, end)


# generated random seed for coordinates
# in order to create pseudo-realistic
# motion behaviour, it accepts additional
# margins to restrict cursor motion beyond
# that marked area
def random_coordinates(limit_x_min, limit_x, limit_y_min, limit_y):
    return random.randrange(limit_x_min, limit_x-limit_x_min),\
           random.randrange(limit_y_min, limit_y-limit_y_min)


# generated random seed for coordinates
# in order to create pseudo-realistic
# scroll behaviour
def random_scroll_amount():
    return random.random() * 100


class TimeDoctorDrivenDevelopment:
    # defines current display size
    def __init__(self):
        self.limit_x_min = self.limit_y_min = safe_frame
        self.limit_x, self.limit_y = pyautogui.size()

    def random_movement(self):
        pyautogui.moveTo(random_coordinates(self.limit_x_min,
                                            self.limit_x-self.limit_x_min,
                                            self.limit_y_min,
                                            self.limit_y-self.limit_y_min),
                         duration=random_time(1, 2))

    @staticmethod
    def random_click():
        pyautogui.click(pyautogui.position(), clicks=1, button='left')

    @staticmethod
    def random_scroll():
        pyautogui.scroll(random_scroll_amount())

    @staticmethod
    def random_typing():
        with open(str(os.path.dirname(os.path.realpath(__file__))) + "/source.txt", "r") as file:

            # calculating total number of lines and
            # picking one of them randomly
            # total_number_of_lines = sum(1 for line in file)
            line_to_print = random_line_number(50)
            for line_number, line in enumerate(file):
                if line_number == line_to_print:
                    pyautogui.typewrite(line, interval=random.choice([0.2, 0.6]))


if __name__ == "__main__":
    time_doctor_worker = TimeDoctorDrivenDevelopment()

    options = [time_doctor_worker.random_movement,
               time_doctor_worker.random_click,
               time_doctor_worker.random_scroll,
               time_doctor_worker.random_typing]

    while True:
        random.choice(options)()
